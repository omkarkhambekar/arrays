function filter(elements,cb){
    const resultArray = [];
    if(elements.length === 0 || !Array.isArray(elements)){
        return [];
    }
    for(let index = 0; index < elements.length; index++){
        if(cb(elements[index],index,elements) === true){
            resultArray.push(elements[index]);
        }
    }
    return resultArray;
}
module.exports= filter;