function each(elements,cb){
    if(elements.length === 0 || !Array.isArray(elements)){
        return [];
    }
    for(let index = 0; index<elements.length; index++){
        cb(elements[index],index);
    }       
}
module.exports = each;