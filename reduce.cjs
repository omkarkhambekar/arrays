function reduce(elements, cb, startingValue){
    let acc;
    let startingIndex;
    if(startingValue !== undefined){
        acc = startingValue;
        startingIndex = 0;
    }
    else{
        acc = elements[0];
        startingIndex = 1;
    }
    if(elements.length === 0){
        return [];
    }

    for(let index = startingIndex; index < elements.length; index++){
        acc = cb(acc,elements[index],index,elements);
    }
    return acc;
}
module.exports = reduce;
