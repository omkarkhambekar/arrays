function flattenArray(elements,depth){
    let outputArr = [];
    
        if(depth === undefined){
            depth = 1;
        }   
        function Helper(elements,outputArr,start,depth){
            for(let index = 0; index < elements.length; index++){
                if(index in elements){
                    if(Array.isArray(elements[index]) && depth){
                        Helper(elements[index],outputArr,0,depth-1);
                    }
                    else{
                        outputArr.push(elements[index]);
                    }
                }
            }
        }
        Helper(elements,outputArr,0,depth);
    return outputArr;
}
module.exports = flattenArray;